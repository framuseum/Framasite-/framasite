<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Async\AsyncAccountGrav;
use AppBundle\Entity\Blog\Blog;
use AppBundle\Entity\Blog\BlogUser;
use AppBundle\Entity\Site;
use Tests\AppBundle\FramasitesTestCase;

class SiteControllerTest extends FramasitesTestCase {

    public function testViewSite()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $blogSite = new Blog($this->getLoggedInUser());
        $blogSite->setSiteName('My awesome blog')->setSiteDescription('My blog is so awesome')->setSiteKeywords('blog, awesome, tag')->setSubdomain('awesome');

        $blogUser = new BlogUser($blogSite);
        $blogUser->setAdmin(true)->setFullName('Loto Winner')->setUsername('MyAwesomeUsername')->setPassword('4w3s0m3');

        /** @var AsyncAccountGrav $accountGrav */
        $accountGrav = $client->getContainer()->get(AsyncAccountGrav::class);
        $accountGrav->setLogger($client->getContainer()->get('logger'))->setUser($this->getLoggedInUser());
        $accountGrav->setSite($blogSite)->setSiteUser($blogUser);
        $accountGrav->make();

        $this->getEntityManager()->persist($blogSite);
        $this->getEntityManager()->flush();

        $crawler = $client->request('GET', '/site/' . $blogSite->getId());

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertGreaterThan(1, $body = $crawler->filter('body')->extract(['_text']));
        $this->assertContains($blogSite->getSiteName(), $body[0]);
        $this->assertContains($blogSite->getSiteDescription(), $body[0]);
        $this->assertContains('tag', $body[0]);
        $this->assertContains($blogUser->getUsername(), $body[0]);
    }

    public function testviewNonExistingSite()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $client->request('GET', '/site/99');

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    public function testIndexAndNewAndDeleteBlog()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertEquals(3, $crawler->filter('div.site-block')->count());

        $newSiteLink = $crawler->filter('#create_new_website')->eq(0)->link();

        $crawler = $client->click($newSiteLink);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertContains('site.choice.head.desc', $crawler->filter('body')->extract(['_text'])[0]);

        $newBlogLink = $crawler->filter('.uk-card-primary')->eq(2)->filter('.btn-primary')->eq(0)->link();

        $crawler = $client->click($newBlogLink);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertContains('site.new.informations.site_keywords.desc', $crawler->filter('body')->extract(['_text'])[0]);

        $form = $crawler->filter('button[id=blog_save]')->form();

        $data = [
            'blog[subdomain]' => 'myawesomewebsite',
            'blog[siteName]' => 'My Awesome Website',
            'blog[siteDescription]' => 'My Awesome Website is an awesome blog',
            'blog[siteKeywords]' => 'Website, keyword, blog',
            'blog[siteUsers][email]' => 'me@about.tld',
            'blog[siteUsers][username]' => 'me',
            'blog[siteUsers][password][first]' => 'longp4ss',
            'blog[siteUsers][password][second]' => 'longp4ss',
            'blog[siteUsers][fullName]' => 'My Name'
        ];

        $client->submit($form, $data);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $crawler = $client->followRedirect();

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertContains('My Awesome Website', $crawler->filter('body')->extract(['_text'])[0]);

        $manageLink = $crawler->filter('a.site-management')->eq(0)->link();

        $crawler = $client->click($manageLink);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertContains('My Awesome Website', $crawler->filter('body')->extract(['_text'])[0]);
    }

    public function testIndexAndNewAndDeleteSinglePage()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertEquals(4, $crawler->filter('div.site-block')->count());

        $newSiteLink = $crawler->filter('#create_new_website')->eq(0)->link();

        $crawler = $client->click($newSiteLink);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertContains('site.choice.head.desc', $crawler->filter('body')->extract(['_text'])[0]);

        $newSinglePageLink = $crawler->filter('.uk-card-primary')->eq(0)->filter('.btn-primary')->eq(0)->link();

        $crawler = $client->click($newSinglePageLink);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertContains('site.new.informations.site_keywords.desc', $crawler->filter('body')->extract(['_text'])[0]);

        $form = $crawler->filter('button[id=single_page_save]')->form();

        $data = [
            'single_page[subdomain]' => 'myawesomewebpage',
            'single_page[siteName]' => 'My Awesome Webpage',
            'single_page[siteDescription]' => 'My Awesome Webpage has been created with PrettyNoemieCMS',
            'single_page[siteKeywords]' => 'Website, keyword, Page',
            'single_page[siteUsers][username]' => 'me',
            'single_page[siteUsers][password][first]' => 'longp4ss',
            'single_page[siteUsers][password][second]' => 'longp4ss',
        ];

        $client->submit($form, $data);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $crawler = $client->followRedirect();

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertContains('My Awesome Webpage', $crawler->filter('body')->extract(['_text'])[0]);

        $manageLink = $crawler->filter('a.site-management')->eq(0)->link();

        $crawler = $client->click($manageLink);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertContains('My Awesome Webpage', $crawler->filter('body')->extract(['_text'])[0]);
    }

    public function addUserToSite()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        /** @var Blog $site */
        $site = $this->getEntityManager()->getRepository('AppBundle:Blog\Blog')->findOneBySubdomain('awesome');

        /** View the page of the site */
        $crawler = $client->request('GET', '/site/' . $site->getId());

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $addUserLink = $crawler->filter('#blog_add_user_button')->eq(0)->link();

        $crawler = $client->click($addUserLink);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertContains('site.new.user.info.general', $crawler->filter('body')->extract(['_text'])[0]);

        $form = $crawler->filter('button[id=blog_user_submit]')->form();

        $data = [
            'blog_user[username]' => 'me', // I already exist !
            'blog_user[password][first]' => 'mycomplicatedpass',
            'blog_user[password][second]' => 'mycomplicatedpass',
        ];

        $client->submit($form, $data);

        $this->assertContains('site.user.already_exists', $crawler->filter('.alert.alert-danger')->extract(['_text'])[0]);

        $data = [
            'blog_[siteUsers][username]' => 'myself',
            'blog_user[password][first]' => 'mycomplicatedpass',
            'blog_user[password][second]' => 'mycomplicatedpass',
        ];

        $client->submit($form, $data);

        $crawler = $client->followRedirect();

        $this->assertEquals(200, $client->getResponse()->getStatusCode());


        $this->assertContains('flashes.site.user.added', $crawler->filter('body')->extract(['_text'])[0]);

        $this->assertCount(2, $crawler->filter('tbody tr'));
    }

    public function removeUserFromSite()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        /** @var Blog $site */
        $site = $this->getEntityManager()->getRepository('AppBundle:Blog\Blog')->findOneBySubdomain('awesome');

        /** View the page of the site */
        $crawler = $client->request('GET', '/site/' . $site->getId());

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $removeUserLink = $crawler->filter('tbody td a')->eq(0)->link();

        $crawler = $client->click($removeUserLink);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $crawler = $client->followRedirect();

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertContains('flashes.site.user.deleted', $crawler->filter('body')->extract(['_text'])[0]);
    }

    /**
     * Delete the site
     */
    public function testDeleteSite()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        /** @var Blog $site */
        $site = $this->getEntityManager()->getRepository('AppBundle:Blog\Blog')->findOneBySubdomain('awesome');

        /** View the page of the site */
        $crawler = $client->request('GET', '/site/' . $site->getId());

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        /** Click on delete button */
        $link = $crawler->filter('div.uk-card-footer a.btn-danger')->eq(0)->link();

        $crawler = $client->click($link);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertContains('site.delete.warning.main', $crawler->filter('body')->extract(['_text'])[0]);

        $form = $crawler->filter('button[id=delete_site_form_save]')->form();

        $data = [
            'delete_site_form[password]' => 'mypassword',
        ];

        $client->submit($form, $data);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $crawler = $client->followRedirect();

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertContains('flashes.site.deleted', $crawler->filter('body')->extract(['_text'])[0]);

        $this->assertEquals(4, $crawler->filter('div.site-block')->count());
    }

    public function testNotAuthenticated()
    {
        $this->logInAs('bob');
        /** @var Site $site */
        $site = $this->getEntityManager()->getRepository('AppBundle:Blog\Blog')->findOneBySubdomain('myawesomewebsite');
        $client = $this->getClient();

        /** View the page of the site */
        $client->request('GET', '/site/' . $site->getId());

        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testDeleteSiteFiles()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        /** @var Site $site */
        $site = $this->getEntityManager()->getRepository('AppBundle:Blog\Blog')->findOneBySubdomain('myawesomewebsite');

        /** @var AsyncAccountGrav $accountGrav */
        $accountGrav = $client->getContainer()->get(AsyncAccountGrav::class);
        $accountGrav->setLogger($client->getContainer()->get('logger'))->setUser($this->getLoggedInUser());
        $accountGrav->setSite($site);
        $accountGrav->deleteSite();

        $this->assertTrue($accountGrav->checkSubdomain());

        $this->getEntityManager()->remove($site);
        $this->getEntityManager()->flush();
    }
}
