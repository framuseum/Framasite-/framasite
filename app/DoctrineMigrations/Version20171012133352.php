<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Initial migration
 */
class Version20171012133352 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE operation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE site_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE cert_task_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE domain_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE command_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE framasite_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE operation (id INT NOT NULL, user_id INT DEFAULT NULL, gandi_id INT NOT NULL, eta INT NOT NULL, status INT NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1981A66DA76ED395 ON operation (user_id)');
        $this->addSql('CREATE TABLE site (id INT NOT NULL, user_id INT DEFAULT NULL, subdomain VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, type INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_694309E4A76ED395 ON site (user_id)');
        $this->addSql('CREATE TABLE cert_task (id INT NOT NULL, user_id INT DEFAULT NULL, status INT NOT NULL, task_data JSON NOT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_39D1D5E6A76ED395 ON cert_task (user_id)');
        $this->addSql('CREATE TABLE domain (id INT NOT NULL, site_id INT DEFAULT NULL, user_id INT DEFAULT NULL, last_gandi_operation_id INT DEFAULT NULL, domain_name VARCHAR(255) NOT NULL, registered_by_frama BOOLEAN NOT NULL, registered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, expires_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, status INT DEFAULT NULL, dns_zone INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A7A91E0BF6BD1646 ON domain (site_id)');
        $this->addSql('CREATE INDEX IDX_A7A91E0BA76ED395 ON domain (user_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A7A91E0BFF956B22 ON domain (last_gandi_operation_id)');
        $this->addSql('CREATE TABLE command (id INT NOT NULL, user_id INT DEFAULT NULL, domain_id INT DEFAULT NULL, amount DOUBLE PRECISION NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, status INT NOT NULL, payplug_id VARCHAR(255) DEFAULT NULL, next VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8ECAEAD4A76ED395 ON command (user_id)');
        $this->addSql('CREATE INDEX IDX_8ECAEAD4115F0EE5 ON command (domain_id)');
        $this->addSql('CREATE TABLE framasite_user (id INT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled BOOLEAN NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, roles TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, locale VARCHAR(255) NOT NULL, name VARCHAR(255) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, gandi_id VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_399E9F9F92FC23A8 ON framasite_user (username_canonical)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_399E9F9FA0D96FBF ON framasite_user (email_canonical)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_399E9F9FC05FB297 ON framasite_user (confirmation_token)');
        $this->addSql('COMMENT ON COLUMN framasite_user.roles IS \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE operation ADD CONSTRAINT FK_1981A66DA76ED395 FOREIGN KEY (user_id) REFERENCES framasite_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE site ADD CONSTRAINT FK_694309E4A76ED395 FOREIGN KEY (user_id) REFERENCES framasite_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cert_task ADD CONSTRAINT FK_39D1D5E6A76ED395 FOREIGN KEY (user_id) REFERENCES framasite_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE domain ADD CONSTRAINT FK_A7A91E0BF6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE domain ADD CONSTRAINT FK_A7A91E0BA76ED395 FOREIGN KEY (user_id) REFERENCES framasite_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE domain ADD CONSTRAINT FK_A7A91E0BFF956B22 FOREIGN KEY (last_gandi_operation_id) REFERENCES operation (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE command ADD CONSTRAINT FK_8ECAEAD4A76ED395 FOREIGN KEY (user_id) REFERENCES framasite_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE command ADD CONSTRAINT FK_8ECAEAD4115F0EE5 FOREIGN KEY (domain_id) REFERENCES domain (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE domain DROP CONSTRAINT FK_A7A91E0BFF956B22');
        $this->addSql('ALTER TABLE domain DROP CONSTRAINT FK_A7A91E0BF6BD1646');
        $this->addSql('ALTER TABLE command DROP CONSTRAINT FK_8ECAEAD4115F0EE5');
        $this->addSql('ALTER TABLE operation DROP CONSTRAINT FK_1981A66DA76ED395');
        $this->addSql('ALTER TABLE site DROP CONSTRAINT FK_694309E4A76ED395');
        $this->addSql('ALTER TABLE cert_task DROP CONSTRAINT FK_39D1D5E6A76ED395');
        $this->addSql('ALTER TABLE domain DROP CONSTRAINT FK_A7A91E0BA76ED395');
        $this->addSql('ALTER TABLE command DROP CONSTRAINT FK_8ECAEAD4A76ED395');
        $this->addSql('DROP SEQUENCE operation_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE site_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE cert_task_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE domain_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE command_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE framasite_user_id_seq CASCADE');
        $this->addSql('DROP TABLE operation');
        $this->addSql('DROP TABLE site');
        $this->addSql('DROP TABLE cert_task');
        $this->addSql('DROP TABLE domain');
        $this->addSql('DROP TABLE command');
        $this->addSql('DROP TABLE framasite_user');
    }
}
