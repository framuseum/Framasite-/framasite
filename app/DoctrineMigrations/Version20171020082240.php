<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Change address into street_address & postal code & city
 */
class Version20171020082240 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE framasite_user ADD zip_code VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE framasite_user ADD city VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE framasite_user RENAME COLUMN address TO street_address');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE framasite_user ADD address VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE framasite_user DROP street_address');
        $this->addSql('ALTER TABLE framasite_user DROP zip_code');
        $this->addSql('ALTER TABLE framasite_user DROP city');
    }
}
