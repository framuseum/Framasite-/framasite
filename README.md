# Framasites

## Support

Merci de [créer un ticket](https://framagit.org/framasoft/Framasite-framasite/issues/new) en étant le plus exhaustif possible. N'hésitez pas à ajouter des captures d'écran.

## Installation

### Prérequis

* `git`
* `nodejs >= v8.x` (mais `6.x` devrait fonctionner)
* `php >= 7.0`
* `PostgreSQL >= 9.6`

### Instructions

#### Production

> Note: Framasites n'est pas considéré comme stable et le manque de mainteneurs et de temps met potentiellement en péril la sécurité de l'application. En conséquence, il n'est **vraiment** pas conseillé de l'installer sans d'abord comprendre le fonctionnement global de l'application.

* Cloner le dépôt ;
* Créer les dossiers à l'intérieur `blog/users` et `wiki/users` ;
* Télécharger les versions forkées de [grav](https://framagit.org/framasoft/Framasite-/framasite-grav) et [dokuwiki](https://framagit.org/framasoft/Framasite-/framasite-doku) et les mettre dans les dossiers `grav` et `dokuwiki` ;
* Créer une base de données PostgreSQL et un utilisateur dédié.
* `composer install --no-dev` (installer les dépendances PHP - [installer Composer](https://getcomposer.org/)) Cette commande demandera également la valeur de nombreux paramètres, comme les informations de base de données définies au point précédent ;
* `npm i` (installer les dépendances Javascript) ;
* `npm run encore production` (précompiler les fichiers JS)
* Éditer dans `app/config/config.yml` les valeurs qui ne sont pas sous forme d'un paramètre (en cours de correction) ;
* `bin/console doctrine:schema:create` ;
* `bin/console doctrine:migrations:migrate` ;
* Créer un vhost nginx sur le modèle de https://symfony.com/doc/3.4/setup/web_server_configuration.html#nginx
* Installer et configurer [Framasite-bot](https://framagit.org/framasoft/Framasite-/framasite-bot) qui s'occupe de récupérer les certifcats Let's Encrypt pour chaque sous-domaine, et générer des configurations de vhost nginx pour chaque sous-domaine/site, y compris les domaines personnalisés rattachés.

#### Développement
Ces instructions sont valables uniquement pour un environnement de développement.

* Cloner le dépôt ;
* Créer les dossiers à l'intérieur `blog/users` et `wiki/users` ;
* Télécharger grav et dokuwiki et les mettre dans les dossiers `grav` et `dokuwiki` ;
* `composer install` (installer les dépendances PHP - [installer Composer](https://getcomposer.org/)) Cette commande demandera également la valeur de nombreux paramètres ;
* `npm i` (installer les dépendances Javascript) ;
* Éditer dans `app/config/config.yml` les valeurs qui ne sont pas sous forme d'un paramètre (en cours de correction) ;
* `bin/console doctrine:database:create` (si base de données pas déjà créée, et utilisateur avec les droits)
* `bin/console doctrine:schema:create` ;
* `bin/console server:run` : rend Framasites accessible à l'adresse http://127.0.0.1:8080.

### Mise à jour

* `git pull` : récupération du dernier code
* `bin/console doctrine:migrations:migrate` migration de la base de données
* `bin/console cache:clear` Nettoie le cache
