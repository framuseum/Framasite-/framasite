<?php

namespace AppBundle\Command;

use AppBundle\Entity\Domain;
use AppBundle\Exception\DomainException\IncorrectDomainConfigurationException;
use AppBundle\Exception\SiteException\UnknownSiteType;
use AppBundle\Service\Domain\AttachDomainService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AttachDomainToSite extends ContainerAwareCommand
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var AttachDomainService
     */
    private $attachDomainService;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $entityManager, AttachDomainService $attachDomainService)
    {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->attachDomainService = $attachDomainService;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('framasites:site:attach-domain')
            ->setDescription('Attach a domain to a website')
            ->setDefinition([
                                new InputArgument('subdomain', InputArgument::REQUIRED, "The website's subdomain, without http or www prefixes"),
                                new InputArgument('domainName', InputArgument::REQUIRED, 'The domain name which should be attached'),
                            ])
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $siteSubdomain = $input->getArgument('subdomain');
        $domainName = $input->getArgument('domainName');

        try {
            $this->checkIfDomainIsReady($domainName);
        } catch (IncorrectDomainConfigurationException $e) {
            if ($e->getErrorType() === IncorrectDomainConfigurationException::ERROR_TYPE_RESERVED) {
                $io->error("Domain name can't be frama.site or frama.wiki");
            } else {
                $io->error("This domain isn't configured properly to point on Framasite's server");
            }
            return;
        }

        $parts = explode('.', $siteSubdomain);

        if ($parts[2] === 'site') {
            $site = $this->entityManager->getRepository('AppBundle:Blog\Blog')->findOneBy(['subdomain' => $parts[0]]);
            if (!$site) {
                $site = $this->entityManager->getRepository('AppBundle:SinglePage\SinglePage')->findOneBy(['subdomain' => $parts[0]]);
            }
        } elseif ($parts[2] === 'wiki') {
            $site = $this->entityManager->getRepository('AppBundle:Wiki\Wiki')->findOneBy(['subdomain' => $parts[0]]);
        } else {
            $io->error("Site subdomain should end with .site or .wiki");
            return;
        }

        if (!$site) {
            $io->error("No such site with this subdomain has been found");
            return;
        }

        $domain = $this->entityManager->getRepository('AppBundle:Domain')->findOneBy(['domainName' => $domainName]);

        /**
         * If the domain is already attached to this particular site
         */
        if (null !== $domain && $domain->getSite() === $site) {
            $io->error("This domain is already attached to this site");
            return;
        }

        /**
         * If the domain is already attached to another site
         */
        if (null !== $domain && $domain->getSite() != null) {
            $io->error("This domain is already attached to another site. Detach it and attach it back.");
            return;
        }


        $domain = new Domain();
        $domain->setUser($site->getUser())
            ->setRegisteredByFrama(false)
            ->setRegisteredAt(new \DateTime())
            ->setDomainName($domainName)
            ->setSite($site)
            ->setStatus(Domain::DOMAIN_WAITING_CONFIGURATION)
        ;
        $site->addDomain($domain);

        try {

            $this->attachDomainService->attachDomain($domain, $site->getUser());

            $io->text('Created an ACTION_CREATE Cert Task to attach our domain');

            $io->success('Domain ' . $domainName . ' has been attached to site ' . $siteSubdomain);
        } catch (UnknownSiteType $e) {
            $io->error($e->getMessage());
            $io->error("This is very strange");
        }
    }

    /**
     * @param string $domain
     * @return bool
     * @throws IncorrectDomainConfigurationException
     * TODO : Move me somewhere else with DomainController::checkIfDomainIsReady
     */
    private function checkIfDomainIsReady(string $domain): bool
    {
        if ($domain === 'frama.site' || $domain === 'frama.wiki') {
            throw new IncorrectDomainConfigurationException(IncorrectDomainConfigurationException::ERROR_TYPE_RESERVED);
        }

        $ipv4 = dns_get_record($domain, DNS_A);
        $ipv6 = dns_get_record($domain, DNS_AAAA);

        if (!$ipv4) {
            throw new IncorrectDomainConfigurationException(IncorrectDomainConfigurationException::ERROR_TYPE_MISSING, IncorrectDomainConfigurationException::IP_V4);
        }

        if (!$ipv6) {
            throw new IncorrectDomainConfigurationException(IncorrectDomainConfigurationException::ERROR_TYPE_MISSING, IncorrectDomainConfigurationException::IP_V6);
        }

        if (dns_get_record("frama.site", DNS_A)[0]['ip'] !== $ipv4[0]['ip']) {
            throw new IncorrectDomainConfigurationException(IncorrectDomainConfigurationException::ERROR_TYPE_WRONG, IncorrectDomainConfigurationException::IP_V4);
        }

        if (dns_get_record("frama.site", DNS_AAAA)[0]['ipv6'] !== $ipv6[0]['ipv6']) {
            throw new IncorrectDomainConfigurationException(IncorrectDomainConfigurationException::ERROR_TYPE_WRONG, IncorrectDomainConfigurationException::IP_V6);
        }
        return true;
    }
}
