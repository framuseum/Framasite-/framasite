<?php

namespace AppBundle\Command;

use AppBundle\Exception\APIException\Release\ReleaseContactException;
use AppBundle\Exception\APIException\Release\ReleaseDomainException;
use AppBundle\Exception\SiteException\UnknownSiteType;
use AppBundle\Service\Domain\DetachDomainService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DetachDomain extends ContainerAwareCommand
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var DetachDomainService
     */
    private $detachDomainService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(LoggerInterface $logger,
                                EntityManagerInterface $entityManager,
                                DetachDomainService $detachDomainService
    ) {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->detachDomainService = $detachDomainService;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('framasites:site:detach-domain')
            ->setDescription("Detach a domain from it's website")
            ->setDefinition(
                [
                    new InputArgument(
                        'domainName', InputArgument::REQUIRED, 'The domain name which should be detached'
                    ),
                ]
            )
            ->setHelp("The domain will be removed from our database. If it was registered with Gandi through Framasoft, it will be send to the user informations to get the domain back.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $domainName = $input->getArgument('domainName');
        $this->logger->info("Wanting to detach " . $domainName . ' from console command');

        $domain = $this->entityManager->getRepository('AppBundle:Domain')->findOneBy(['domainName' => $domainName]);

        if (!$domain) {
            $io->error("No such domain has been found inside Framasites");
            return;
        }

        try {
            $this->detachDomainService->detachDomain($domain, $domain->getUser());
            $io->success('Domain ' . $domainName . ' has been successfully detached');
        } catch (ReleaseDomainException $e) {
            $io->error('When releasing domain ' . $domain->getDomainName() . ' from console command, an error occurred : ' . $e->getMessage());
        } catch (ReleaseContactException $e) {
            $io->error('When releasing contact ' . $e->getGandiId() . ' from console command, an error occurred : ' . $e->getMessage());
        } catch (UnknownSiteType $e) {
            $io->error($e->getMessage());
            $io->error("Unknown Site type : very odd !");
        }

    }
}
