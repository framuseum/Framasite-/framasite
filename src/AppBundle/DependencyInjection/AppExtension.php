<?php

namespace AppBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class AppExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('app.path_blog_accounts', $config['blog']['path_accounts']);
        $container->setParameter('app.path_grav', $config['blog']['path_grav']);
        $container->setParameter('app.blogs_limit', $config['blog']['limit_per_user']);
        $container->setParameter('app.path_wiki_accounts', $config['wiki']['path_accounts']);
        $container->setParameter('app.path_doku', $config['wiki']['path_doku']);
        $container->setParameter('app.wikis_limit', $config['wiki']['limit_per_user']);
        $container->setParameter('app.path_simple_page_accounts', $config['single_page']['path_accounts']);
        $container->setParameter('app.path_pretty_noemie', $config['single_page']['path_pretty_noemie']);
        $container->setParameter('app.single_page_limit', $config['single_page']['limit_per_user']);
        $container->setParameter('app.gandi.api.mode', $config['gandi']['mode']);
        $container->setParameter('app.gandi.default_zone', $config['gandi']['mode'] === 'test' ? $config['gandi']['default_zone']['test'] : $config['gandi']['default_zone']['prod']);
        $container->setParameter('app.gandi.api_key.test', $config['gandi']['key']['test']);
        $container->setParameter('app.gandi.api_key.prod', $config['gandi']['key']['prod']);
        $container->setParameter('app.gandi.api.id.test', $config['gandi']['id']['test']);
        $container->setParameter('app.gandi.api.id.prod', $config['gandi']['id']['prod']);
        $container->setParameter('app.stripe_api_mode', $config['stripe']['mode']);
        $container->setParameter('app.stripe_api_key_test_private', $config['stripe']['key']['test']['private']);
        $container->setParameter('app.stripe_api_key_test_public', $config['stripe']['key']['test']['public']);
        $container->setParameter('app.stripe_api_key_prod_private', $config['stripe']['key']['prod']['private']);
        $container->setParameter('app.stripe_api_key_prod_public', $config['stripe']['key']['prod']['public']);
        $container->setParameter('app.languages', $config['languages']);
        $container->setParameter('app.framasoft.marge', $config['framasoft']['marge']);
        $container->setParameter('app.framasoft.bank.iban', $config['framasoft']['bank']['iban']);
        $container->setParameter('app.framasoft.bank.bic', $config['framasoft']['bank']['bic']);
        $container->setParameter('app.framasoft.bank.name', $config['framasoft']['bank']['owner_name']);
        $container->setParameter('app.framasoft.bank.address', $config['framasoft']['bank']['owner_address']);
        $container->setParameter('app.framasoft.address', $config['framasoft']['address']);
        $container->setParameter('app.framasoft.email', $config['framasoft']['email']);
        $container->setParameter('app.engines_enabled', $config['sites_enabled']);
        $container->setParameter('app.registration.enabled', $config['registration']['enabled']);
        $container->setParameter('app.registration.suffix', $config['registration']['suffix']);
        $container->setParameter('app.framasoft.buy_domains', $config['framasoft']['buy_domains']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
    }

    public function getAlias()
    {
        return 'app';
    }
}
