<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ConstraintValidSiteConfiguration extends Constraint
{
    public $alreadyExistsSiteMessage = 'site.already_exists';

    public function validatedBy()
    {
        return ConfigurationSiteValidator::class;
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
