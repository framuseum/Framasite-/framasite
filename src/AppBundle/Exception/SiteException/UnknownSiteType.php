<?php

namespace AppBundle\Exception\SiteException;

class UnknownSiteType extends \Exception {

    protected $type;

    /**
     * UnknownSiteType constructor.
     * @param string $message
     * @param string $type
     */
    public function __construct(string $type, string $message = "")
    {
        parent::__construct($message);
        $this->type = $type;
    }

    public function getType(): string
    {
        return $this->type;
    }
}