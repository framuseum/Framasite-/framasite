<?php

namespace AppBundle\Exception;

class SiteUserExistsException extends \Exception
{
    /** @var string */
    private $siteFolder;

    /**
     * @var string
     */
    private $siteUser;

    /**
     * SiteUserExistsException constructor.
     * @param string $siteFolder
     * @param string $siteUser
     * @param string $message
     */
    public function __construct(string $siteFolder, string $siteUser, $message = "")
    {
        parent::__construct($message);
        $this->siteFolder = $siteFolder;
        $this->siteUser = $siteUser;
    }

    /**
     * @return string
     */
    public function getSiteFolder(): string
    {
        return $this->siteFolder;
    }

    /**
     * @return string
     */
    public function getSiteUser(): string
    {
        return $this->siteUser;
    }
}
