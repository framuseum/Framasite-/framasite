<?php

namespace AppBundle\Entity\Wiki;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Site;

/**
 * @ORM\Entity()
 */
class Wiki extends Site
{
    const POLICY_OPEN = 0;
    const POLICY_PUBLIC = 1;
    const POLICY_CLOSED = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     */
    protected $siteName;

    /**
     * @var string
     */
    protected $siteDescription;

    /**
     * @var int
     */
    protected $policy;

    /**
     * @return string
     */
    public function getSiteName()
    {
        return $this->siteName;
    }

    /**
     * @param string $siteName
     * @return Wiki
     */
    public function setSiteName(string $siteName): Wiki
    {
        $this->siteName = $siteName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSiteDescription()
    {
        return $this->siteDescription;
    }

    /**
     * @param string $siteDescription
     * @return Wiki
     */
    public function setSiteDescription(string $siteDescription): Wiki
    {
        $this->siteDescription = $siteDescription;
        return $this;
    }

    /**
     * @return int
     */
    public function getPolicy()
    {
        return $this->policy;
    }

    /**
     * @param int $policy
     * @return Wiki
     */
    public function setPolicy(int $policy): Wiki
    {
        $this->policy = $policy;
        return $this;
    }

    public function __toString()
    {
        return $this->subdomain . '.frama.wiki';
    }
}
