<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="command")
 * @ORM\HasLifecycleCallbacks()
 */
class Command
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"domain_register"})
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="amount", type="float")
     * @Assert\NotBlank()
     * @var float
     * @Groups({"domain_register"})
     */
    private $amount;

    /**
     * @ORM\Column(name="total", type="float")
     * @Assert\NotBlank()
     * @var float
     * @Groups({"domain_register"})
     */
    private $total;

    /**
     * @var float
     *
     * @ORM\Column(name="cut", type="float")
     * @Groups({"domain_register"})
     */
    private $cut = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\NotBlank()
     * @Groups({"domain_register"})
     */
    private $createdAt;

    /**
     * @ORM\Column(name="status", type="integer")
     * @var int
     * @Assert\Range(
     *     min="0",
     *     max="50",
     * )
     */
    private $status = self::STATUS_CREATED;

    const STATUS_CREATED = 0;
    const STATUS_SEND = 10;
    const STATUS_RESPONDED = 20;
    const STATUS_WAITING_CONFIGURATION = 30;
    const STATUS_COMMAND_CANCELLED = 40;
    const STATUS_PAYMENT_CANCELLED = 42;
    const STATUS_PAYMENT_ISSUE = 43;
    const STATUS_FINISHED = 50;
    const STATUS_PARTIALLY_FINISHED = 51;
    const STATUS_FINISHED_PROCESSED = 60;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="commands")
     * @Groups({"domain_register"})
     */
    private $user;

    /**
     * When user is deleted, keep it's username
     *
     * @ORM\Column(name="username", type="string", nullable=true)
     * @var string
     * @Groups({"domain_register"})
     */
    private $username;

    /**
     * When user is deleted, we keep it's email
     *
     * @ORM\Column(name="email", type="string", nullable=true)
     * @var string
     * @Groups({"domain_register"})
     */
    private $email;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CommandDomain", mappedBy="command")
     */
    private $commandDomains;

    /**
     * If domain is deleted, we keep it's name
     *
     * @ORM\Column(name="domain_names", type="string", nullable=true)
     * @var string
     * @Groups({"domain_register"})
     */
    private $domainNames;

    /**
     * @var string
     *
     * @ORM\Column(name="stripe_id", type="string", nullable=true)
     * @Groups({"domain_register"})
     */
    private $stripeId;

    /**
     * @var int
     *
     * @ORM\Column(name="payment_type", type="integer", nullable=true)
     * @Groups({"domain_register"})
     */
    private $paymentType;

    const PAYMENT_TYPE_DIRECT = 1;
    const PAYMENT_TYPE_SAVED_CARD = 2;
    const PAYMENT_TYPE_BANKWIRE = 3;

    /**
     * Next route to go after payment success
     *
     * @var string
     *
     * @ORM\Column(name="next", type="string")
     */
    private $next = 'domain-fill';

    /**
     * Command constructor.
     */
    public function __construct()
    {
        $this->status = self::STATUS_CREATED;
        $this->createdAt = new \DateTime();
        $this->commandDomains = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Command
     */
    public function setId(int $id): Command
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return float
     * @Assert\NotEqualTo(0)
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return Command
     */
    public function setAmount(float $amount = null): Command
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     * @return Command
     */
    public function setTotal(float $total): Command
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return float
     */
    public function getCut()
    {
        return $this->cut;
    }

    /**
     * @param float $cut
     * @return Command
     */
    public function setCut(float $cut): Command
    {
        $this->cut = $cut;
        return $this;
    }

    /**
     * @return User|string
     */
    public function getUser()
    {
        return $this->user ?: $this->username;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        $this->username = $user->getUsername();
        $this->email = $user->getEmail();
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Command
     */
    public function setStatus(int $status): Command
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getCommandDomains()
    {
        return $this->commandDomains;
    }

    /**
     * @param Collection $commandDomains
     * @return Command
     */
    public function setCommandDomains(Collection $commandDomains): Command
    {
        $this->commandDomains = $commandDomains;
        $this->calculateDomainName();
        return $this;
    }

    /**
     * @param CommandDomain $commandDomain
     * @return Command
     */
    public function addCommandDomain(CommandDomain $commandDomain): Command
    {
        $this->commandDomains->add($commandDomain);
        $this->calculateDomainName();
        $this->calculateSums();
        return $this;
    }

    /**
     * @param CommandDomain $commandDomain
     * @return Command
     */
    public function removeCommandDomain(CommandDomain $commandDomain): Command
    {
        $this->commandDomains->removeElement($commandDomain);
        $this->calculateDomainName();
        $this->calculateSums();
        return $this;
    }

    /**
     * Calculate domain name from domains
     */
    private function calculateDomainName()
    {
        $domainNamesArray = [];
        foreach ($this->commandDomains as $commandDomain) {
            $domainNamesArray[] .= $commandDomain->getDomain()->getDomainName();
        }
        $this->domainNames = implode(', ', $domainNamesArray);
    }

    /**
     * Recalculate sums from command domains
     */
    private function calculateSums()
    {
        $this->amount = 0;
        $this->total = 0;
        $this->cut = 0;
        foreach ($this->commandDomains as $commandDomain) {
            /** @var $commandDomain CommandDomain */
            $this->amount += $commandDomain->getAmount();
            $this->total += $commandDomain->getTotal();
            $this->cut += $commandDomain->getCut();
        }
    }

    /**
     * @return array
     */
    public function getDomains(): array
    {
        $domains = [];
        foreach ($this->commandDomains as $commandDomain) {
            $domains[] = $commandDomain->getDomain();
        }
        return $domains;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Command
     */
    public function setCreatedAt(\DateTime $createdAt): Command
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getStripeId()
    {
        return $this->stripeId;
    }

    /**
     * @param string $stripeId
     * @return Command
     */
    public function setStripeId(string $stripeId): Command
    {
        $this->stripeId = $stripeId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @param int $paymentType
     * @return Command
     */
    public function setPaymentType(int $paymentType): Command
    {
        $this->paymentType = $paymentType;
        return $this;
    }

    /**
     * @return string
     */
    public function getNext(): string
    {
        return $this->next;
    }

    /**
     * @param string $next
     * @return Command
     */
    public function setNext(string $next): Command
    {
        $this->next = $next;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return Command
     */
    public function setUsername(string $username): Command
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Command
     */
    public function setEmail(string $email): Command
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getDomainNames()
    {
        return $this->domainNames;
    }

    /**
     * @param string $domainNames
     * @return Command
     */
    public function setDomainNames(string $domainNames): Command
    {
        $this->domainNames = $domainNames;
        return $this;
    }
}
