<?php

namespace AppBundle\Entity;

class Mailbox
{

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var array
     */
    private $aliases;

    /**
     * @var string
     */
    private $fallbackEmail;

    /**
     * @var array
     */
    private $quota;

    /**
     * @var array
     */
    private $responder;

    /**
     * @var Domain
     */
    private $domain;

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     * @return Mailbox
     */
    public function setLogin(string $login): Mailbox
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Mailbox
     */
    public function setPassword(string $password): Mailbox
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return $this->aliases;
    }

    /**
     * @param array $aliases
     * @return Mailbox
     */
    public function setAliases(array $aliases): Mailbox
    {
        $this->aliases = $aliases;
        return $this;
    }

    /**
     * @return string
     */
    public function getFallbackEmail()
    {
        return $this->fallbackEmail;
    }

    /**
     * @param string $fallbackEmail
     * @return Mailbox
     */
    public function setFallbackEmail($fallbackEmail): Mailbox
    {
        $this->fallbackEmail = $fallbackEmail;
        return $this;
    }

    /**
     * @return array
     */
    public function getQuota()
    {
        return $this->quota;
    }

    /**
     * @param array $quota
     * @return Mailbox
     */
    public function setQuota(array $quota): Mailbox
    {
        $this->quota = $quota;
        return $this;
    }

    /**
     * @return array
     */
    public function getResponder()
    {
        return $this->responder;
    }

    /**
     * @param array $responder
     * @return Mailbox
     */
    public function setResponder(array $responder): Mailbox
    {
        $this->responder = $responder;
        return $this;
    }

    /**
     * @return Domain
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param Domain $domain
     * @return Mailbox
     */
    public function setDomain(Domain $domain): Mailbox
    {
        $this->domain = $domain;
        return $this;
    }
}
