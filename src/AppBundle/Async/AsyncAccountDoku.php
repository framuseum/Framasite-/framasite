<?php

namespace AppBundle\Async;

use AppBundle\Entity\Wiki\Wiki;
use AppBundle\Entity\Wiki\WikiUser;
use AppBundle\Exception\Misc\BcryptNotInstalledException;
use AppBundle\Exception\SiteException\SiteCreationException;
use AppBundle\Exception\SiteException\SiteDeletionException;
use AppBundle\Exception\SiteException\UserSiteException\UserSiteDeletionException;
use AppBundle\Exception\SiteUserExistsException;
use AppBundle\Exception\SubDomainFolderExistsException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Yaml\Yaml;

class AsyncAccountDoku extends AbstractMakeAccount implements AsyncAccountInterface
{
    /**
     * @var Wiki
     */
    protected $site;

    /**
     * @var WikiUser
     */
    protected $siteUser;

    /**
     * @var string Path to Config of the user
     */
    protected $configPath;

    /**
     * @var string Path to Data of the user
     */
    protected $dataPath;

    /**
     * Execute the async job
     *
     * @throws SiteCreationException
     */
    public function make()
    {
        try {
            $this->logger->info(
                'Preparing to create wiki subdomain ' . $this->site->getSubdomain(
                ) . 'and account ' . $this->siteUser->getUsername() . ' for user ' . $this->user->getUsername() . '!'
            );
            $this->configPath = $this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.wiki' . '/conf';
            $this->checkSubdomain();
            $this->makeSubdomain();

            $this->makeSiteConfig();
            $this->makeAccount();

            $this->logger->info(
                'Created subdomain ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!'
            );
        } catch (SubDomainFolderExistsException $e) {
            $this->logger->error("The subdomain " . $e->getFolder() . " already exists. Aborting");
            throw new SiteCreationException();
        } catch (BcryptNotInstalledException $e) {
            $this->logger->error("BCrypt is not installed, we can't hash passwords");
            throw new SiteCreationException();
        } catch (SiteUserExistsException $e) {
            $this->logger->error("The user " . $e->getSiteUser() . " already exists for site " . $e->getSiteFolder());
            throw new SiteCreationException();
        }
    }

    /**
     * @param string|null $subdomain
     * @return bool
     * @throws SubDomainFolderExistsException
     */
    public function checkSubdomain(string $subdomain = null)
    {
        $this->logger->info('Checking subdomain ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
        $subdomainToCheck = null === $subdomain ? $this->site->getSubdomain() . '.frama.wiki' : $subdomain;
        $fs = new Filesystem();
        $accountPath = $this->accountsPath . '/' . strtolower($subdomainToCheck);
        if ($fs->exists($accountPath)) {
            throw new SubDomainFolderExistsException($accountPath, "Folder already exists");
        }
        return true;
    }

    /**
     * Create the folders for a subdomain
     */
    public function makeSubdomain()
    {
        $this->logger->info('Preparing subdomain creation ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
        $fs = new Filesystem();
        $accountPath = $this->accountsPath . '/' . strtolower($this->site->getSubdomain() . '.frama.wiki');
        $fs->mkdir($accountPath);

        $userData    = ['conf', 'data', 'data/cache'];

        // Create userData folders
        foreach ($userData as $file) {
            $fs->mkdir($accountPath . '/' . $file);
            $fs->chmod($this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.wiki' . '/' . $file, 0755);
        }

        /**
         * Copy conf
         */
        $fs->copy($this->accountsPath . '/../template/conf/local.protected.php', $this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.wiki' . '/conf/local.protected.php');
        $fs->copy($this->accountsPath . '/../template/conf/users.auth.php', $this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.wiki' . '/conf/users.auth.php');
        $fs->copy($this->accountsPath . '/../template/conf/plugins.local.php', $this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.wiki' . '/conf/plugins.local.php');

        /**
         * Copy data
         */
        $this->rcopy($this->accountsPath . '/../template/data', $this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.wiki' . '/data');
        $this->logger->info('Created subdomain ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
    }

    /**
     *
     */
    public function makeSiteConfig()
    {
        $this->logger->info('Creating config for site ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
        /**
         * Global configuration
         */
        $conf['title'] = addslashes($this->site->getSiteName());
        $conf['tagline'] = addslashes($this->site->getSiteDescription());
        $conf['lang'] = addslashes($this->user->getLocale());
        $conf['license'] = 'cc-by-sa';
        $conf['useacl'] = 1;
        $conf['superuser'] = '@admin';
        $conf['passcrypt'] = 'bcrypt';
        $conf['userewrite'] = 1;

        $configFilePath = $this->configPath . '/local.php';

        $fs = new Filesystem();
        if (!$fs->exists($this->configPath)) {
            $fs->mkdir($this->configPath);
        }

        // backup current file (remove any existing backup)
        if ($fs->exists($configFilePath)) {
            if ($fs->exists($configFilePath.'.bak')) {
                $fs->remove($configFilePath . '.bak');
            }
            $fs->rename($configFilePath, $configFilePath.'.bak');
        }

        if (!fopen($configFilePath, 'wb')) {
            $fs->rename($configFilePath.'.bak', $configFilePath);     // problem opening, restore the backup
            return false;
        }

        $data = $this->saveSettings($conf);

        file_put_contents($configFilePath, $data);
        $fs->chmod($configFilePath, 0755);

        /**
         * ACL configuration
         */

        $output =
<<<EOT
# acl.auth.php
# <?php exit()?>
# Don't modify the lines above
#
# Access Control Lists
#
# Auto-generated by install script
# Date: 
EOT;
        $now = date('r');
        $output .= $now . PHP_EOL;

        if ($this->site->getPolicy() == Wiki::POLICY_CLOSED) {
            $output .=  "*               @ALL          0\n";
            $output .=  "*               @user         8\n";
        } elseif ($this->site->getPolicy() == Wiki::POLICY_PUBLIC) {
            $output .=  "*               @ALL          1\n";
            $output .=  "*               @user         8\n";
        } else {
            $output .=  "*               @ALL          8\n";
        }
        file_put_contents($this->configPath . '/acl.auth.php', $output);
        $fs->chmod($this->configPath . '/acl.auth.php', 0755);


        $output = <<<EOT
<?php
/*
 * Local plugin enable/disable settings
 *
 * Auto-generated by install script
 * Date: $now
 */

\$plugins['authad']    = 0;
\$plugins['authldap']  = 0;
\$plugins['authmysql'] = 0;
\$plugins['authpgsql'] = 0;

EOT;

        file_put_contents($this->configPath . '/plugins.local.php', $output);
        $fs->chmod($this->configPath . '/plugins.local.php', 0755);

        $this->logger->info('Created config for site ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
    }

    /**
     * @param string|null $username
     * @param string|null $subdomain
     * @return bool
     * @throws \Exception|SiteUserExistsException
     */
    public function checkAccount(string $username = null, string $subdomain = null)
    {
        $usernameToCheck = null != $username ? $username : $this->siteUser->getUsername();
        $subdomainToCheck = null != $subdomain ? $subdomain : $this->site->getSubdomain();

        $this->logger->info('Checking account ' . $usernameToCheck . ' for site ' . $subdomainToCheck . ' for user ' . $this->user->getUsername() . '!');
        $this->configPath = $this->accountsPath . '/' . $subdomainToCheck . '.frama.wiki' . '/conf';

        /**
         * Read each line from the user file and check if it starts with the username to check
         */
        $handle = fopen($this->configPath . '/users.auth.php', "r");
        $found = false;
        if ($handle) {
            while ((($line = fgets($handle)) !== false) && !$found) {
                $found = 0 === strpos($line, $username . ':');
            }

            fclose($handle);
        } else {
            throw new FileException("Unable to open user file");
        }
        if ($found) {
            throw new SiteUserExistsException($subdomainToCheck, $usernameToCheck, "User already exists !");
        }

        return true;
    }

    /**
     * @throws SiteUserExistsException
     * @throws BcryptNotInstalledException
     */
    public function makeAccount()
    {
        $this->logger->info('Creating account ' . $this->siteUser->getUsername() . ' for site ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
        $this->checkAccount();

        // just in case
        $this->configPath = $this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.wiki' . '/conf';

        // hash the password
        $pass = $this->hash_bcrypt($this->siteUser->getPassword());

        // create users.auth.php
        // --- user:SMD5password:Real Name:email:groups,comma,seperated
        $output = join(":", [$this->siteUser->getUsername(), $pass, $this->siteUser->getFullName(), $this->siteUser->getEmail(), 'admin,user']);

        $output = file_get_contents($this->configPath .'/users.auth.php') . "\n$output\n";

        file_put_contents($this->configPath . '/users.auth.php', $output);
        $fs = new Filesystem();
        $fs->chmod($this->configPath . '/users.auth.php', 0755);

        $this->logger->info('Created account ' . $this->siteUser->getUsername() . ' for site ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
    }

    /**
     * @throws SiteDeletionException
     */
    public function deleteSite()
    {
        $this->logger->info('Deleting site at subdomain ' . $this->site->getSubdomain() . '.frama.wiki' . '...');
        $fs = new Filesystem();
        if (null == $this->site->getSubdomain() . '.frama.wiki' || $this->site->getSubdomain() . '.frama.wiki' == '') { // TODO : check against ../../ and such things
            throw new SiteDeletionException("Subdomain is empty, won't delete everything !");
        }
        $fs->remove($this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.wiki');
    }

    /**
     * Deletes an user from a site
     *
     * @throws UserSiteDeletionException
     */
    public function deleteSiteUser()
    {
        $this->logger->info($this->user->getUsername() . " tried to delete account " . $this->siteUser->getUsername() . " for website " . $this->site->getSubdomain() . '.frama.wiki');

        $fs = new Filesystem();
        $finder = new Finder();

        if (!$fs->exists($this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.wiki' . '/user/accounts' . $this->siteUser->getUsername() . '.yaml')) {
            throw new UserSiteDeletionException("This user " . $this->siteUser->getUsername() . " doesn't exist. Strange");
        }
        $files = $finder->files()->in($this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.wiki' . '/user/accounts')->name('*.yaml');
        foreach ($files as $file) {
            /** @var SplFileInfo $file */
            $userData = Yaml::parse($file->getContents());

            $this->logger->debug("Testing if account " . $file->getRelativePathname() . " is admin");

            /** There's at least one more admin, we can remove this user */
            if (isset($userData['access']['admin']) && null !== $userData['access']['admin'] && $file->getRelativePathname() !== $this->siteUser->getUsername()) {
                $this->logger->info("The site user " . $file . " is an admin, so we can delete the site user " . $this->siteUser->getUsername());
                $fs->remove($this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.wiki' . '/user/accounts/' . $this->siteUser->getUsername() . '.yaml');
                $this->logger->info($this->user->getUsername() . " deleted account " . $this->siteUser->getUsername() . " for website " . $this->site->getSubdomain() . '.frama.wiki');
            }
        }
    }


    /**
     * Stores setting[] array to file
     *
     * @param array $settings settings
     * @return string
     */
    public function saveSettings(array $settings)
    {
        $out = $this->outHeader();

        foreach ($settings as $settingName => $settingValue) {
            $out .= $this->out($settingName, $settingValue);
        }

        $out .= $this->outFooter();

        return $out;
    }

    /**
     * Generate string to save setting value to file according to $fmt
     *
     * @param $key
     * @param $value
     * @return string
     */
    public function out($key, $value)
    {
        if (!isset($value)) {
            $value = '';
        }
        return '$conf["'.$key.'"] = "'. $this->cleanText($value).'";' . PHP_EOL;
    }

    /**
     * Returns header of rewritten settings file
     *
     * @return string text of header
     */
    private function outHeader()
    {
        return '<'.'?php'."\n".
            "/*\n".
            " * Auto-generated by Framasites\n".
            " * Run for user: ".$this->user->getUsername()."\n".
            " * Date: ".date('r')."\n".
            " */\n\n";
    }

    /**
     * Returns footer of rewritten settings file
     *
     * @return string text of footer
     */
    private function outFooter()
    {
        return "\n// end auto-generated content\n";
    }

    /**
     * convert line ending to unix format
     *
     * also makes sure the given text is valid UTF-8
     *
     * @see    formText() for 2crlf conversion
     * @author Andreas Gohr <andi@splitbrain.org>
     *
     * @param string $text
     * @return string
     */
    private function cleanText(string $text)
    {
        $text = preg_replace("/(\015\012)|(\015)/", "\012", $text);

        // if the text is not valid UTF-8 we simply assume latin1
        // this won't break any worse than it breaks with the wrong encoding
        // but might actually fix the problem in many cases
        if (!mb_detect_encoding($text, 'UTF-8', true)) {
            $text = utf8_encode($text);
        }

        return $text;
    }

    /**
     * Passwordhashing method 'bcrypt'
     *
     * Uses a modified blowfish algorithm called eksblowfish
     * This method works on PHP 5.3+ only and will throw an exception
     * if the needed crypt support isn't available
     *
     * A full hash should be given as salt (starting with $a2$) or this
     * will break. When no salt is given, the iteration count can be set
     * through the $compute variable.
     *
     * @param string $clear The clear text to hash
     * @param string|null $salt The salt to use, null for random
     * @param int $compute The iteration count (between 4 and 31)
     * @return string Hashed password
     * @throws BcryptNotInstalledException
     */
    public function hash_bcrypt(string $clear, string $salt = null, int $compute = 8): string
    {
        if (!defined('CRYPT_BLOWFISH') || CRYPT_BLOWFISH != 1) {
            throw new BcryptNotInstalledException('This PHP installation has no bcrypt support');
        }

        if (is_null($salt)) {
            if ($compute < 4 || $compute > 31) {
                $compute = 8;
            }
            $salt = '$2a$'.str_pad((string) $compute, 2, '0', STR_PAD_LEFT).'$'.
                $this->gen_salt(22);
        }

        return crypt($clear, $salt);
    }

    /**
     * Create a random salt
     *
     * @param int $len The length of the salt
     * @return string
     */
    public function gen_salt(int $len = 32): string
    {
        $salt  = '';
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        for ($i = 0; $i < $len; $i++) {
            $salt .= $chars[$this->random(0, 61)];
        }
        return $salt;
    }

    /**
     * Use DokuWiki's secure random generator if available
     *
     * @param int $min
     * @param int $max
     * @return int
     */
    protected function random(int $min, int $max): int
    {
        return random_int($min, $max);
    }

    /**
     * @param string $source
     * @param string $link
     */
    public function linkDomainFolder(string $source, string $link)
    {
        $this->logger->info('Symlinking ' . $this->accountsPath . '/' . $source . '.frama.wiki' . ' and ' . $this->accountsPath . '/' . $link);
        $sourcePath = $this->accountsPath . '/' . $source . '.frama.wiki';
        $targetPath = $this->accountsPath . '/' . $link;
        Tools::relativeSymlink($sourcePath, $targetPath);
    }

    /**
     * @param string $domain
     */
    public function unlinkDomainFolder(string $domain)
    {
        $fs = new Filesystem();
        $this->logger->info('Unlinking ' . $this->accountsPath . '/' . $domain);
        $fs->remove($this->accountsPath . '/' . $domain);
    }
}
