<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class UserRepository extends EntityRepository
{
    /**
     * Find a user by its username.
     *
     * @param string $username
     *
     * @return User
     */
    public function findOneByUserName($username)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.username = :username')->setParameter('username', $username)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Count how many users are enabled.
     *
     * @return int
     */
    public function getSumEnabledUsers()
    {
        return $this->createQueryBuilder('u')
            ->select('count(u)')
            ->andWhere('u.enabled = true')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Retrieves users filtered with a search term.
     *
     * @param string $term
     *
     * @return QueryBuilder
     */
    public function getQueryBuilderForSearch($term)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('lower(u.username) LIKE lower(:term) OR lower(u.email) LIKE lower(:term)')->setParameter('term', '%' . $term . '%');
    }
}
