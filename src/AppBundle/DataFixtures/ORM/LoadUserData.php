<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setFirstName('Super')
            ->setLastName('Admin')
            ->setEmail('superadmin@framasoft.org')
            ->setUsername('admin')
            ->setPlainPassword('mypassword')
            ->setEnabled(true)
            ->addRole('ROLE_SUPER_ADMIN');

        $manager->persist($userAdmin);

        $this->addReference('admin-user', $userAdmin);

        $bobUser = new User();
        $bobUser->setFirstName('Bobby')
            ->setLastName('Lapointe')
            ->setEmail('bobby@framasoft.org')
            ->setUsername('bob')
            ->setPlainPassword('mypassword')
            ->setEnabled(true)
            ->setLocale('en')
            ->setGandiId('EX123-GANDI');

        $manager->persist($bobUser);

        $this->addReference('bob-user', $bobUser);

        $emptyUser = new User();
        $emptyUser->setFirstName('Empty')
            ->setLastName('Inside me')
            ->setEmail('empty@framasoft.org')
            ->setUsername('empty')
            ->setPlainPassword('mypassword')
            ->setEnabled(true);

        $manager->persist($emptyUser);

        $this->addReference('empty-user', $emptyUser);

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 10;
    }
}
