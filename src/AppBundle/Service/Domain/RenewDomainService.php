<?php

namespace AppBundle\Service\Domain;

use AppBundle\Entity\Command;
use AppBundle\Entity\CommandDomain;
use AppBundle\Entity\Domain;
use AppBundle\Entity\User;
use AppBundle\Exception\APIException;
use AppBundle\Exception\APIException\Renew\APIIssueRenewException;
use AppBundle\Exception\APIException\Renew\DomainNotOKRenewException;
use AppBundle\Exception\APIException\Renew\NotReadyRenewException;
use AppBundle\Helper\DomainFactory;
use AppBundle\Service\BaseService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class RenewDomainService extends BaseService
{
    /**
     * @var DomainFactory
     */
    private $domainFactory;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $entityManager, DomainFactory $domainFactory)
    {
        parent::__construct($logger, $entityManager);
        $this->domainFactory = $domainFactory;
    }

    public function renewDomain(Domain $domain, User $user, float $framarge): Command
    {

        /**
         * We can only renew the domains we registered
         */
        if (!$domain->isRegisteredByFrama()) {
            $this->logger->error('User ' . $user->getUsername() . " tried to renew the domain . " . $domain->getDomainName() . "that isn't from us");
            throw new AccessDeniedException("You can only renew frama domains");
        }

        /**
         * The domain isn't OK so we can't renew it
         */
        if ($domain->getStatus() !== Domain::DOMAIN_OK) {
            throw new DomainNotOKRenewException();
        }

        /**
         * Let's try to get the renewal date of the domain
         */
        try {
            $domainData = $this->domainFactory->infoDomain($domain);
        } catch (APIException $e) {
            throw new APIIssueRenewException();
        }

        /**
         * Check if the renewal period has begun
         */
        if (new \DateTime($domainData['date_renew_begin']) >= new \DateTime()) {
            throw new NotReadyRenewException("Domain is not yet due for renewal", $domainData['date_renew_begin']);
        }

        /**
         * Let's calculate the price for renewal
         */
        $amount = $this->domainFactory->calculatePriceForAction($domain->getDomainName(), 1, 'domains', 'renew');

        $margeAmount = round($amount * $framarge, 2);
        $total = $amount + $margeAmount;

        $this->logger->debug("Now here 5 with amount " . $amount . " and total " . $total);

        /**
         * Let's create a new command
         */
        $command = new Command($user);
        $commandDomain = new CommandDomain($amount, $total);
        $commandDomain->setDomain($domain)->setCommand($command);
        $command->addCommandDomain($commandDomain);

        $this->logger->info('Created new command and new command domain for renewal');

        $command->setNext('domain-renew-confirm');
        $this->entityManager->persist($command);
        $this->entityManager->flush();

        $this->logger->debug("Now here 6");

        return $command;
    }
}
