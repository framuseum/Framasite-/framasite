<?php

namespace AppBundle\Controller\BuyDomains;

use AppBundle\Entity\Command;
use AppBundle\Entity\User;
use AppBundle\Exception\PaymentException;
use AppBundle\Helper\PaymentFactory;
use Dompdf\Dompdf;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommandController extends Controller
{
    /**
     * @var PaymentFactory
     */
    private $paymentFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger, PaymentFactory $paymentFactory)
    {
        $this->paymentFactory = $paymentFactory;
        $this->logger = $logger;
    }

    /**
     * @Route("/command", name="command-index")
     *
     * @return Response
     */
    public function indexCommandAction()
    {
        return $this->render('default/command/index.html.twig', [
            'commands' => $this->getUser()->getCommands(),
        ]);
    }

    /**
     * @Route("/command/{command}", name="command-view")
     *
     * @param Command $command
     * @return Response
     */
    public function paymentSummaryAction(Command $command): Response
    {
        $this->checkUserAction($command);
        $marge = $this->getParameter('app.framasoft.marge');
        return $this->render('default/command/view.html.twig', [
            'command' => $command,
            'marge' => $marge,
        ]);
    }

    /**
     * @Route("/command/{command}/finish", name="command-finish")
     *
     * @param Command $command
     * @return Response
     * @throws PaymentException
     */
    public function finishCommandAction(Command $command): Response
    {
        return $this->render('default/domain/success.html.twig', ['domain' => $command->getDomains()[0]]);
    }

    /**
     * @Route("/command/{command}/close", name="command-close")
     *
     * @param Command $command
     * @return Response
     */
    public function closeCommandAction(Command $command): Response
    {
        if ($command->getStatus() < Command::STATUS_COMMAND_CANCELLED) {
            $command->setStatus(Command::STATUS_COMMAND_CANCELLED);
            /** @var User $user */
            $user = $this->getUser();
            $user->setActiveCommand(null);
            $em = $this->getDoctrine()->getManager();
            $em->persist($command);
            $em->persist($user);
            $em->flush();
            $this->logger->info('User ' . $user->getUsername() . " closed it's command n°" . $command->getId());
        }
        return $this->redirectToRoute('command-view', ['command' => $command->getId()]);
    }

    /**
     * @Route("/command/{command}/invoice", name="command-invoice")
     *
     * @param Command $command
     * @return Response
     */
    public function generateInvoiceAction(Command $command): Response
    {
        $this->checkUserAction($command);
        $framasoftAddress = $this->getParameter('app.framasoft.address');
        return $this->render('default/command/invoice.html.twig', ['command' => $command, 'framasoftAddress' => $framasoftAddress, 'pdf' => false]);
    }

    /**
     * @Route("/command/{command}/invoice/pdf", name="command-invoice-pdf")
     *
     * @param Command $command
     * @return Response
     */
    public function generatePdfInvoiceAction(Command $command): Response
    {
        $this->checkUserAction($command);
        $framasoftAddress = $this->getParameter('app.framasoft.address');

        $html = $this->renderView('default/command/invoice.html.twig', ['command' => $command, 'framasoftAddress' => $framasoftAddress, 'pdf' => true]);

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, ['toc' => false]),
            $command->getDomainNames() . '.pdf'
        );
    }


    /**
     * Cancel a command
     * Sets command status to cancelled and abort payment
     *
     * @Route("/command/{command}/cancel", name="command-cancel")
     *
     * @param Command $command
     * @return Response
     */
    public function cancelCommandAction(Command $command): Response
    {
        $this->checkUserAction($command);

        $em = $this->getDoctrine()->getManager();

        $command->setStatus(Command::STATUS_COMMAND_CANCELLED);

        /** @var User $user */
        $user = $this->getUser();
        $user->setActiveCommand();

        $em->persist($command);
        $em->persist($user);
        $em->flush();
        $this->logger->info('User ' . $user->getUsername() . " cancelled it's command n°" . $command->getId());
        return $this->redirectToRoute('command-index');
    }

    /**
     * @param Command $command
     */
    private function checkUserAction(Command $command)
    {
        if ((null === $this->getUser() || $this->getUser()->getId() !== $command->getUser()->getId()) && !($this->getUser()->hasRole('ROLE_ADMIN') || $this->getUser()->hasRole('SUPER_ADMIN'))) {
            throw $this->createAccessDeniedException('You can not access this command.');
        }
    }
}
