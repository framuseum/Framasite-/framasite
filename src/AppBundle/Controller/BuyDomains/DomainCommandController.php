<?php

namespace AppBundle\Controller\BuyDomains;

use AppBundle\Entity\Command;
use AppBundle\Entity\CommandDomain;
use AppBundle\Entity\Contact;
use AppBundle\Entity\Domain;
use AppBundle\Entity\User;
use AppBundle\Exception\APIException;
use AppBundle\Exception\LimitationException;
use AppBundle\Form\ContactType;
use AppBundle\Form\GetDomainType;
use AppBundle\Helper\DomainFactory;
use AppBundle\Service\Command\CommandService;
use Doctrine\Common\Annotations\AnnotationReader;
use JMS\Serializer\SerializationContext;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\JsonSerializableNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\PropertyNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Translation\TranslatorInterface;

class DomainCommandController extends Controller
{
    const OFFERED_EXTENSIONS = [
        'fr', 'ru', 'de', 'jp', 'co.uk', 'it', 'pl', 'in', 'ir', 'nl', 'es', 'ch', 'be', 'se', 'us', // countries
        'com', 'net', 'info', 'org', 'eu', 'biz', 'me', 'tv', 'io', 'xyz', 'cc', 'top', 'pro' // generic
    ];

    /**
     * @var DomainFactory
     */
    private $domainFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * @var CommandService
     */
    private $commandService;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(
        LoggerInterface $logger,
                                FlashBagInterface $flashBag,
                                TranslatorInterface $translator,
                                CommandService $commandService,
                                DomainFactory $domainFactory
    ) {
        $this->logger = $logger;
        $this->flashBag = $flashBag;
        $this->translator = $translator;
        $this->domainFactory = $domainFactory;
        $this->commandService = $commandService;
    }

    /**
     * @Route("/domain/help", name="domain-help")
     *
     * @return Response
     */
    public function domainHelpGeneralAction(): Response
    {
        return $this->render('default/domain/help/help_general.html.twig', ['buy_domains' => $this->getParameter('app.framasoft.buy_domains')]);
    }

    /**
     * @Route("/domain/suggest/{domainAsked}", name="domain-suggest")
     *
     * @param string $domainAsked
     * @return JsonResponse
     */
    public function getSuggestedDomains(string $domainAsked): JsonResponse
    {
        try {
            if ($domainAsked === 'undefined') {
                return new JsonResponse("Can't check undefined. Fix your shit", 400);
            }
            $this->logger->info('Finding domains for '. $domainAsked);

            $availabilities = $this->domainFactory->getSuggestedDomains(
                $domainAsked,
                self::OFFERED_EXTENSIONS,
                $this->getParameter('app.framasoft.marge'),
                $this->getUser()->getUsername()
            );
            return new JsonResponse($availabilities);
        } catch (LimitationException $e) {
            return new JsonResponse(['error' => 'Limitation', 'message' => $e->getMessage()], 401);
        } catch (APIException $e) {
            return new JsonResponse(['error' => 'API', 'message' => $e->getMessage()], 400);
        }
    }

    /**
     * @Route("/domain/price", name="price-domain")
     *
     * @return Response
     * @throws APIException
     */
    public function showDomainPriceAction(): Response
    {
        $extensions = [];
        $marge = $this->getParameter('app.framasoft.marge');

        $cache = new FilesystemAdapter();
        $cachedPrices = $cache->getItem('domain.prices');
        $cachedPrices->expiresAfter(\DateInterval::createFromDateString('1 day'));
        if (!$cachedPrices->isHit()) {
            foreach (self::OFFERED_EXTENSIONS as $extension) {
                $amount = $this->domainFactory->calculatePriceForAction('.' . $extension);
                $extensions[$extension] = $amount + round($amount * $marge, 2);
            }
            $cachedPrices->set($extensions);
            $cache->save($cachedPrices);
        }
        $extensions = $cachedPrices->get();

        return $this->render('default/domain/price.html.twig', ['extensions' => $extensions]);
    }

    /**
     * @Route("/domain/new", name="add-domain")
     *
     * @param Request $request
     * @return Response
     * @throws APIException
     */
    public function addDomainAction(Request $request): Response
    {
        if ($this->getParameter('app.framasoft.buy_domains') === false) {
            throw new AccessDeniedException("You can't buy domains at this time");
        }

        /** @var User $user */
        $user = $this->getUser();

        /**
         * Set basic informations about the domain
         */
        $domain = new Domain();
        $domain->setUser($user);

        $domain->setRegisteredByFrama(true);

        $getDomainForm = $this->createForm(GetDomainType::class, $domain);
        $getDomainForm->handleRequest($request);

        if ($getDomainForm->isSubmitted() && $getDomainForm->isValid()) {
            $this->getDoctrine()->getManager()->persist($domain);
            $user->addDomain($domain);
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $marge = $this->getParameter('app.framasoft.marge');
            $command = $this->commandService->newCommand($domain, $this->getUser(), $marge);

            return $this->redirectToRoute('command-view', [
                'command' => $command->getId(),
            ]);
        }
        return $this->render(':default/domain:new.html.twig', [
            'form' => $getDomainForm->createView(),
        ]);
    }

    /**
     * This shows the page to create a BuyDomains contact and sets up a basic contact
     *
     * @Route("/domain/fill/{command}", name="domain-fill", requirements={"command": "\d+"}))
     *
     * @param Request $request
     * @param Command $command
     * @return Response
     * @throws \Exception
     */
    public function fillDomainContactAction(Request $request, Command $command): Response
    {
        if ($this->getParameter('app.framasoft.buy_domains') === false) {
            throw new AccessDeniedException("You can't buy domains at this time");
        }

        $this->checkAuthForCommand($command);

        /**
         * Remaining command domains
         */
        $commandDomainsRemaining = $command->getCommandDomains()->filter(function (CommandDomain $commandDomain) {
            return $commandDomain->getStatus() < CommandDomain::STATUS_CREATED;
        });

        if ($commandDomainsRemaining->count() === 0) {
            $this->logger->error("No command domains remaining, we should be here");
            return $this->redirectToRoute('command-finish', ['command' => $command->getId()]);
        }

        /** @var CommandDomain $currentCommandDomain */
        $currentCommandDomain = $commandDomainsRemaining->first();
        $currentDomain = $currentCommandDomain->getDomain();
        $currentCommand = $currentCommandDomain->getCommand();
        $this->logger->debug('Current domain command is ', [$currentCommandDomain]);
        $this->logger->debug('Current command is ', [$currentCommand]);

        $userContact = new Contact();
        $userContact->setContactType(Contact::CONTACT_TYPE_OWNER);
        $currentDomain->addContact('owner', $userContact);

        /**
         * If the user already has a Gandi ID associated
         */
        if ($gandiId = $this->getUser()->getGandiId()) {
            $this->logger->info("Using user's own GandiID");
            $userContact->setGandiId($gandiId);

            $message = $this->serializeMessage($currentCommandDomain);
            $this->logger->info('Publishing message', [$message]);
            $this->get('old_sound_rabbit_mq.create_domain_producer')->publish($message);
            return $this->handleNextDomain($command, $currentCommandDomain);
        } else {
            $userContact->setEmail($this->getUser()->getEmail());

            /**
             * Generating a password for the user
             */
            $tokenGenerator = $this->container->get('fos_user.util.token_generator');
            $password = substr($tokenGenerator->generateToken(), 0, 10);
            $userContact->setPassword($password);
            $userContact->setLang($this->getUser()->getLocale());

            $contactForm = $this->createForm(ContactType::class, $userContact, ['user' => $this->getUser()]);
            $contactForm->handleRequest($request);

            if ($contactForm->isSubmitted() && $contactForm->isValid()) {
                $message = $this->serializeMessage($currentCommandDomain);
                $this->logger->info('Publishing message', [$message]);
                $this->get('old_sound_rabbit_mq.create_contact_producer')->publish($message);

                return $this->handleNextDomain($command, $currentCommandDomain);
            }
            $nbDomains = $command->getCommandDomains()->count();
        }

        return $this->render('default/domain/register.html.twig', [
            'form' => $contactForm->createView(),
            'domainName' => $currentDomain->getDomainName(),
            'nbDomains' => $nbDomains,
            'passedDomains' => $nbDomains - $commandDomainsRemaining->count(),
        ]);
    }

    /**
     * @param CommandDomain $commandDomain
     * @return string
     */
    private function serializeMessage(CommandDomain $commandDomain): string
    {
        $serializer = $this->get('jms_serializer');

        return $serializer->serialize($commandDomain, 'json', SerializationContext::create()->setGroups(['domain_register']));
    }

    /**
     * @param Command $command
     * @param CommandDomain $currentCommandDomain
     * @return RedirectResponse
     */
    private function handleNextDomain(Command $command, CommandDomain $currentCommandDomain)
    {
        $this->logger->debug("Set command domain with ID " . $currentCommandDomain->getId() . ' to finished');
        $currentCommandDomain->setStatus(CommandDomain::STATUS_CREATED);

        $this->getDoctrine()->getManager()->persist($currentCommandDomain);
        $this->getDoctrine()->getManager()->flush();

        /**
         * Remaining command domains
         */
        $commandDomainsRemaining = $command->getCommandDomains()->filter(function (CommandDomain $commandDomain) {
            return $commandDomain->getStatus() < CommandDomain::STATUS_CREATED;
        });

        if ($commandDomainsRemaining->count() === 0) { // if we have processed all the domains to buy
            $this->commandService->makePayment($command, $this->getUser());

            $command->setStatus(Command::STATUS_FINISHED);
            $this->logger->info('Command is finished');

            /**
             * Resetting user's active command to null
             */
            $user = $this->getUser();
            $user->setActiveCommand();

            $this->getDoctrine()->getManager()->persist($command);
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('command-finish', ['command' => $command->getId()]);
        } else {
            $command->setStatus(Command::STATUS_PARTIALLY_FINISHED);
            $this->logger->info('We have more domains to process');
            $this->getDoctrine()->getManager()->persist($command);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('domain-fill', ['command' => $command->getId()]);
        }
    }

    /**
     * @Route("/command/{command}/remove-domain/{commandDomain}", name="remove-command-domain-from-command")
     * @param Command $command
     * @param CommandDomain $commandDomain
     * @return RedirectResponse
     */
    public function removeDomainFromCommandAction(Command $command, CommandDomain $commandDomain)
    {
        $this->logger->info("Removed command domain " . $commandDomain->getDomain()->getDomainName() . " from command #" . $command->getId());
        $command->removeCommandDomain($commandDomain);
        $em = $this->getDoctrine()->getManager();
        $em->remove($commandDomain);
        $em->persist($command);
        $em->flush();
        return $this->redirectToRoute('command-view', ['command' => $command->getId()]);
    }

    /**
     * @param Command $command
     * @return bool
     */
    private function checkAuthForCommand(Command $command)
    {
        if ($command->getUser() !== $this->getUser() && !($this->getUser()->hasRole('ROLE_ADMIN') || $this->getUser()->hasRole('SUPER_ADMIN'))) {
            throw new AccessDeniedException("You don't have access to this command");
        }
        return true;
    }
}
