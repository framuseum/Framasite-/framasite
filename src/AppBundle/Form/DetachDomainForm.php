<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class DetachDomainForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', PasswordType::class, [
                'required' => true,
                'label_attr' => ['class' => 'col-sm-2'],
                'label' => 'domain.detach.form.password.label',
                'constraints' => [
                    new UserPassword(),
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'site.delete.form.submit',
                'attr' => ['class' => 'btn btn-primary'],
            ])
        ;
    }
}
