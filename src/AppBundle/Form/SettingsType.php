<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingsType extends AbstractType
{
    private $languages = [];

    /**
     * @param array $languages Languages come from configuration, array just code language as key and label as value
     */
    public function __construct($languages)
    {
        $this->languages = $languages;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale', ChoiceType::class, [
                'choices' => array_flip($this->languages),
                'label' => 'settings.languages.label',
                'label_attr' => ['class' => 'col-sm-2'],
            ])
            ->add('gandiId', TextType::class, [
                'label' => 'settings.gandiId.label',
                'attr' => ['placeholder' => 'AB123-GANDI'],
                'label_attr' => ['class' => 'col-sm-2'],
                'required' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'settings.save',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => User::class,
       ]);
    }

    public function getBlockPrefix()
    {
        return 'config';
    }
}
